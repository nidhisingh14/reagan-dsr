// Copyright (c) 2021, Mainul Islam and contributors
// For license information, please see license.txt

frappe.ui.form.on('Store Inventory', {

	countsheet_type: function (frm) {
		console.log("cvbnmhj");
		if (frm.doc.countsheet_type) {
			frm.clear_table("item_details");
			frappe.call({
				method: "dsr.dsr.doctype.store_inventory.store_inventory.get_item_details",
				
				args: {
					countsheet_type_name : frm.doc.countsheet_type,

				},
				callback: function (r, rt) {
					if (r.message) {
						console.log(r.message);

						$.each(r.message, function (i, d) {

							var c = frm.add_child("item_details");
							c.item_code = d.item_code;
							c.item_group = d.item_group;
							c.whole_uom = d.whole_uom;
							c.broken_uom = d.broken_uom;

							frm.refresh_field("item_details");


						});
					}
				}
			})
		}
	}
});


frappe.ui.form.on('Store Inventory', {

	countsheet_type: function (frm) {
		// console.log("cvbnmhj");
		if (frm.doc.countsheet_type) {
			frm.clear_table("outlet_prep");
			frappe.call({
				method: "dsr.dsr.doctype.store_inventory.store_inventory.get_outlet_details",
				
				args: {
					countsheet_type_name : frm.doc.countsheet_type,

				},
				callback: function (r, rt) {
					if (r.message) {
						console.log(r.message);

						$.each(r.message, function (i, d) {

							var c = frm.add_child("outlet_prep");
							c.item_code = d.item_code;
							c.item_group = d.item_group;
							c.whole_uom = d.whole_uom;
							c.broken_uom = d.broken_uom;

							frm.refresh_field("outlet_prep");


						});
					}
				}
			})
		}
	}
});

