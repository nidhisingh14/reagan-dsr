// // Copyright (c) 2021, Mainul Islam and contributors
// // For license information, please see license.txt


// frappe.ui.form.on('Countsheet Template', {

//     menu_filter: function (frm) {
//         if (frm.doc.menu_filter) {
//             frm.clear_table("items");
//             frappe.call({
//                 method: "dsr.dsr.doctype.countsheet_template.countsheet_template.get_menu",
//                 async: false,
//                 args: {
//                     menu_name: frm.doc.menu_filter,

//                 },
//                 callback: function (r, rt) {
//                     if (r.message) {

//                         $.each(r.message, function (i, d) {

//                             frappe.db.get_value('Item Group', d.item_group, 'outlet_prep_enabled')
//                                 .then(r => {
//                                     if (r.message.outlet_prep_enabled == 0) {

//                                         var c = frm.add_child("items");
//                                         c.item_code = d.item_code;
//                                         c.item_group = d.item_group;
//                                         c.whole_uom = d.stock_uom;

//                                         frappe.call({
//                                             method: "dsr.dsr.doctype.countsheet_template.countsheet_template.get_uom_conversion",
//                                             async: false,
//                                             args: {
//                                                 'item_code': d.item_code
//                                             },

//                                             callback: function (data) {
//                                                 if (data.message) {
//                                                     console.log(data.message)

//                                                     var item = "";
//                                                     for (var i = 0; i < data.message.length; i++) {

//                                                         item = item + "," + data.message[i].uom;


//                                                     }
//                                                     c.item_uom = item.substr(1, 20);

//                                                     var item_str = item.substr(1, 20);


                                                    
//                                                 }
//                                             }
//                                         });

//                                         frm.refresh_field("items");
//                                     }
//                                     else {

//                                         var outlet_prep_child = frm.add_child("outlet_prep");
//                                         outlet_prep_child.item_code = d.item_code;
//                                         outlet_prep_child.item_group = d.item_group;
//                                         outlet_prep_child.whole_uom = d.stock_uom;

//                                         frappe.call({
//                                             method: "dsr.dsr.doctype.countsheet_template.countsheet_template.get_uom_conversion",
//                                             args: {
//                                                 'item_code': d.item_code
//                                             },

//                                             callback: function (data) {
//                                                 if (data.message) {
//                                                     console.log(data.message)

//                                                     var item = "";
//                                                     for (var i = 0; i < data.message.length; i++) {

//                                                         item = item + "," + data.message[i].uom;


//                                                     }
//                                                     outlet_prep_child.item_uom = item.substr(1, 20);

//                                                     var item_str = item.substr(1, 20);


                                                    
//                                                 }
//                                             }
//                                         });
                                        

//                                         frm.refresh_field("outlet_prep");

//                                     }

//                                 });
//                         });
//                     }
//                 }
//             });
//         }
//     }
// });


// frappe.ui.form.on('Countsheet Template', {
//     refresh: function (frm) {
//         if (frappe.session.user != "Administrator") {
//             cur_frm.set_query("menu_filter", function () {
//                 return {
//                     "filters": [
//                         ["Menu Filter", "user_id", "=", frappe.session.user],
//                     ]
//                 };
//             });
//         }
//     },
// });

// cur_frm.set_query("broken_uom", "outlet_prep", function (doc, cdt, cdn) {
//     var d = locals[cdt][cdn];
//     d.item_uom = d.item_uom + '';
//     console.log(d.item_uom);
//     // d.item_uom = d.item_uom.split(',');
//     return {
//         filters: [
//             ['UOM', 'uom_name', 'in', d.item_uom]
//         ]
//     };
// });

// cur_frm.set_query("broken_uom", "items", function (doc, cdt, cdn) {
//     var d = locals[cdt][cdn];
//     d.item_uom = d.item_uom + '';
//     console.log(d.item_uom);
//     // d.item_uom = d.item_uom.split(',');
//     return {
//         filters: [
//             ['UOM', 'uom_name', 'in', d.item_uom]
//         ]
//     };
// });

frappe.ui.form.on('Countsheet Template', {
    menu_filter: function (frm) {
        if (frm.doc.menu_filter) {
            frm.clear_table("items");
            frappe.call({
                method: "dsr.dsr.doctype.countsheet_template.countsheet_template.get_menu",
                async: false,
                args: {
                    menu_name: frm.doc.menu_filter,
                },
                callback: function (r, rt) {
                    if (r.message) {
                        $.each(r.message, function (i, d) {
                            frappe.db.get_value('Item Group', d.item_group, 'outlet_prep_enabled')
                                .then(r => {
                                    if (r.message.outlet_prep_enabled == 0) {
                                        var c = frm.add_child("items");
                                        c.item_code = d.item_code;
                                        c.item_group = d.item_group;
                                        c.whole_uom = d.stock_uom;
                                        frappe.call({
                                            method: "dsr.dsr.doctype.countsheet_template.countsheet_template.get_uom_conversion",
                                            async: false,
                                            args: {
                                                'item_code': d.item_code
                                            },
                                            callback: function (data) {
                                                if (data.message) {
                                                    var item = "";
                                                    for (var i = 0; i < data.message.length; i++) {
                                                        item = item + "," + data.message[i].uom;

                                                    }
                                                    c.item_uom = item.substr(1, 20);

                                                    var item_str = item.substr(1, 20);
                                                }
                                            }
                                        });

                                        frm.refresh_field("items");
                                    }
                                    else {
                                        
                                        frappe.call({
                                            method: 'frappe.client.get_value',
                                            async: false,
                                            args: {
                                                'doctype': 'BOM',
                                                'filters': {'item':d.item_code},
                                                'fieldname': [
                                                    'name'
                                                 ]
                                        },
                                        callback : function(r) {
                                            // add item group into table 'bom discount detail'
                                            if (!r.exc) {
                                                if(r.message.name!=undefined)
                                                {frappe.call({
                                                    method: "dsr.dsr.doctype.countsheet_template.countsheet_template.get_rawmaterial",
                                                    async: false,
                                                    args: {
                                                        'item_code': r.message.name
                                                    },  
                        
                                                callback : function(k) {
                                                  debugger;
                                                   console.log(k.message.length)
                                                   for(i=0;i<k.message.length;i++)
                                                    {
                                                   frappe.call({
                                                    method: 'frappe.client.get_value',
                                                    async: false,
                                                    args: {
                                                        'doctype': 'Item',
                                                        'filters': {'item_code':k.message[i].item_code},
                                                        'fieldname': [
                                                            'item_group',
                                                            'stock_uom'
                                                         ]
                                                },
                                                callback : function(l) {
                                                    var c = frm.add_child("items");
                                                    c.item_code = k.message[i].item_code;
                                                    c.item_group = l.message.item_group;
                                                    c.whole_uom = l.message.stock_uom;
                                                    frappe.call({
                                                        method: "dsr.dsr.doctype.countsheet_template.countsheet_template.get_uom_conversion",
                                                        async: false,
                                                        args: {
                                                            'item_code':k.message[i].item_code
                                                        },
                                                        callback: function (data) {
                                                            if (data.message) {
                                                                var item = "";
                                                                for (var i = 0; i < data.message.length; i++) {
                                                                    item = item + "," + data.message[i].uom;
                                                                }
                                                                c.item_uom = item.substr(1, 20);
            
                                                                var item_str = item.substr(1, 20);
                                                            }
                                                        }
                                                    });
                                                    
                                                }
                                            })
                                        }  
                                    }  
                                            })
                                        }
                                        }
                                    }
                                        })

                                        var outlet_prep_child = frm.add_child("outlet_prep");
                                        outlet_prep_child.item_code = d.item_code;
                                        outlet_prep_child.item_group = d.item_group;
                                        outlet_prep_child.whole_uom = d.stock_uom
                                        frappe.call({
                                            method: "dsr.dsr.doctype.countsheet_template.countsheet_template.get_uom_conversion",
                                            args: {
                                                'item_code': d.item_code
                                            },

                                            callback: function (data) {
                                                if (data.message) {
                                                    //console.log(data.message)
                                                    var item = "";
                                                    for (var i = 0; i < data.message.length; i++) {
                                                        item = item + "," + data.message[i].uom;
                                                    }
                                                    outlet_prep_child.item_uom = item.substr(1, 20);
                                                    var item_str = item.substr(1, 20);
                                                }
                                            }
                                        });

                                        frm.refresh_field("outlet_prep");
                                    }

                                });
                        });
                    }
                }
            });
        }
    }
});


frappe.ui.form.on('Countsheet Template', {
    refresh: function (frm) {
        if (frappe.session.user != "Administrator") {
            cur_frm.set_query("menu_filter", function () {
                return {
                    "filters": [
                        ["Menu Filter", "user_id", "=", frappe.session.user],
                    ]
                };
            });
        }
    },
});

cur_frm.set_query("broken_uom", "outlet_prep", function (doc, cdt, cdn) {
    var d = locals[cdt][cdn];
    d.item_uom = d.item_uom + '';
    //console.log(d.item_uom);
    // d.item_uom = d.item_uom.split(',');
    return {
        filters: [
            ['UOM', 'uom_name', 'in', d.item_uom]
        ]
    };
});

cur_frm.set_query("broken_uom", "items", function (doc, cdt, cdn) {
    var d = locals[cdt][cdn];
    d.item_uom = d.item_uom + '';
    //console.log(d.item_uom);
    // d.item_uom = d.item_uom.split(',');
    return {
        filters: [
            ['UOM', 'uom_name', 'in', d.item_uom]
        ]
    };
});



