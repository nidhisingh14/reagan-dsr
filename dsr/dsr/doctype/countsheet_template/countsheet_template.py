# -*- coding: utf-8 -*-
# Copyright (c) 2021, Mainul Islam and contributors
# For license information, please see license.txt

# from __future__ import unicode_literals
# import frappe
# from frappe.model.document import Document

# class CountsheetTemplate(Document):
	# pass




from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _
from frappe.utils import getdate, nowdate, cint, flt

class CountsheetTemplate(Document):
	pass

@frappe.whitelist()
def get_menu(menu_name):
	item = frappe.db.sql("""
	select *
	from `tabItem` item join `tabItem Menu` item_menu on item_menu.parent=item.name where menu_name=%s
	and item.docstatus=0 """, menu_name, as_dict = 1)

	return item if item else None



@frappe.whitelist()
def get_uom_conversion(item_code):
	item = frappe.db.sql("""
	select uom_conversion_detail.uom, item.item_code
	from `tabUOM Conversion Detail` uom_conversion_detail join `tabItem` item on uom_conversion_detail.parent=item.name where item.item_code=%s
	and item.docstatus=0 """, item_code, as_dict = 1)

	return item if item else None

@frappe.whitelist()
def get_rawmaterial(item_code):
	item_list = frappe.db.sql("""select item_code from `tabBOM Item` where parent=%s and docstatus=1""",(item_code), as_dict = 1)
	return item_list if item_list else None	


# @frappe.whitelist()
# def get_uom_conversion(item_code):
# 	return frappe.db.sql("""
# 				SELECT 
# 					u.uom
# 				FROM `tabUOM Conversion Detail` u, `tabItem` i
# 				WHERE u.parent=i.name
# 				i.item_code= %s
# 		""",(item_code),as_list =True)
