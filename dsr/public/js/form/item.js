frappe.ui.form.on("Item", {
    setup: frm => {
        frm.set_query("default_bin_location", "item_defaults", (frm, cdt, cdn) => {
            let row = frappe.get_doc(cdt, cdn)
            return {
                filters: [["parent", "=", row.default_zone_location]]
            }
        })
    }
})