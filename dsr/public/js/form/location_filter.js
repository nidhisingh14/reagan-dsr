frappe.ui.form.on(cur_frm.doctype, {
    zone_location(frm) {
        if (frm.doc.zone_location) {
            frm.events.show_all_grides(frm, true)
            frm.doc[frm.item_field || 'items'].forEach(row => {
                if (row.zone_location === frm.doc.zone_location) {
                    frm.fields_dict[frm.item_field || 'items'].$wrapper.find(`.rows .grid-row[data-name="${row.name}"]`).removeClass('hide')
                }
            })
        } else {
            frm.events.show_all_grides(frm, false)
        }
    },
    show_all_grides(frm, hide) {
        if (hide) {
            frm.fields_dict[frm.item_field || 'items'].$wrapper.find('.rows .grid-row').addClass('hide')
        } else {
            frm.fields_dict[frm.item_field || 'items'].$wrapper.find('.rows .grid-row').removeClass('hide')
        }
    },
    refresh(frm) {
        frm.events.show_zone_location_summary(frm)
    },
    show_zone_location_summary(frm) {
        if (frm.doc[frm.item_field || 'items'] && frappe.meta.has_field(frm.doctype, 'zone_location_summary')) {
            let summary = {}
            frm.doc[frm.item_field || 'items'].forEach(row => {
                if (row.zone_location) {
                    if (!summary[row.zone_location])
                        summary[row.zone_location] = 0;
                    // Increment Items
                    summary[row.zone_location] += 1
                }
            })
            frm.fields_dict.zone_location_summary.$wrapper.html('<h6>Zone wise Items</h6>')
            let html = ''
            for (let [zon, qty] of Object.entries(summary)) {
                html += `<tr><th>${zon}</th> <td>${qty}</td></tr>`
            }
            frm.fields_dict.zone_location_summary.$wrapper.append(`<table class="table table-bordered">${html}</table>`)
        }
    }
})
