# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "DSR",
			"color": "green",
			"icon": "fa fa-user",
			"type": "module",
			"label": _("DSR")
		}
	]
