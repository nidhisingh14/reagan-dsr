from frappe import _


def get_data():
    return[
        {
            "label": ("Location Management"),
            "items": [{
                    "type": "doctype",
                    "name": "Zone Location",
                    "onboard": 1,
                    "label": _("Zone Location"),
                    "description": _("Managing Warehouse cell"),
                }
            ]
        },


        {
            "label": ("Countsheet"),
            "items": [{
                    "type": "doctype",
                    "name": "Countsheet Template",
                    "onboard": 1,
                    "label": _("Countsheet Template"),
                }
            ]
        },

        {
            "label": ("Store"),
            "items": [{
                    "type": "doctype",
                    "name": "Store Inventory",
                    "onboard": 1,
                    "label": _("Store Inventory"),
                }
            ]
        }
    ]

