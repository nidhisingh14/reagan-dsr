from types import MethodType
from erpnext.stock.doctype.delivery_note.delivery_note import DeliveryNote
import frappe
from frappe.utils.data import flt, round_based_on_smallest_currency_fraction
from dsr.utils.taxes_and_totals import dsr_calculate_taxes_and_totals
from frappe import _


def delivery_note_events(doc, method=None):
    if doc.naming_series not in ("DSR-.YYY.-", "DSR-.YYYY.-"):
        return
    # Set Items property
    if method in ('onload', 'before_validate', 'before_cancel', 'before_update_after_submit', 'before_submit'):
        doc.items = doc.get('pos_items', default=[]) + doc.get('non_pos_items', default=[])
        doc.load_from_db = MethodType(load_from_db, doc)
        # TODO: Remove after parent app update event code by cheking fields.
        if not hasattr(doc, "taxes"):
            doc.taxes = []
        if not hasattr(doc, 'discount_table_items'):
            doc.discount_table_items = []
        if doc.total_discounts is None:
            doc.total_discounts = 0

    # Calculate.
    if method in ('before_save', 'before_submit'):
        update_payment_method_total(doc)
        update_discounts(doc)
        discount_set_in_taxes(doc)
        dsr_calculate_taxes_and_totals(doc)
        set_payment_percent(doc)
        if method == 'before_submit':
            validate_payment_method_total(doc)



def set_payment_percent(doc):
    totals = doc.rounded_total or doc.grand_total
    for row in doc.get('payment_method_item', default=[]):
        row.set('status', flt(row.amount*100/totals, row.precision('status')))


def validate_payment_method_total(doc):
    if (doc.rounded_total or doc.grand_total) != sum(flt(x.amount) for x in doc.get('payment_method_item', default=[])):
        frappe.throw(_("Payment Total should be Equal to {}").format(doc.rounded_total or doc.grand_total))


def load_from_db(self):
    super(DeliveryNote, self).load_from_db()
    self.items = self.get('pos_items', default=[]) + self.get('non_pos_items', default=[])


def discount_set_in_taxes(doc):
    for table_field in ('pos_discount', 'non_pos_discount'):
        total_discount = 0
        for discount_name in set(x.discount_name for x in doc.get(table_field, default=[])):
            amount = sum(x.discount_amount for x in doc.get(
                table_field, {"discount_name": discount_name}, default=[]))
            is_positive = frappe.get_value("Discount", discount_name, 'positive_amount')
            if amount > 0 and not is_positive:
                amount = -1 * amount
            row = doc.get("taxes", {"discount_name": discount_name, "table_field": table_field},
                          default=[])

            account = frappe.get_value("Discount", discount_name, 'account')
            row = row[0] if row else doc.append("taxes",
                                                {"discount_name": discount_name,
                                                 "table_field": table_field,
                                                 "charge_type": "Actual",
                                                 "cost_center": frappe.db.get_value("Company",
                                                                                    doc.company,
                                                                                    "cost_center"),
                                                    "description": account,
                                                    "account_head": account})
            for field in ('tax_amount', 'total', 'tax_amount_after_discount_amount',
                          'base_total', 'base_total'):
                row.set(field, amount)
            total_discount += amount
        doc.set("total_{}".format(table_field), total_discount)
    doc.set("total_discounts", flt(doc.total_pos_discount) + flt(doc.total_non_pos_discount))

def update_sales_taxes_and_charges(doc):
    if not doc.taxes:
        return
    doc.total_taxes_and_charges = doc.taxes[0].tax_amount
    doc.taxes[0].included_in_print_rate = 1

    doc.grand_total_pos = doc.pos_sales_amount + doc.total_pos_discount
    doc.grand_total_non_pos = doc.non_pos_sales_amount + doc.total_non_pos_discount
    doc.grand_total = doc.total + doc.total_discounts
    doc.base_grand_total = flt(doc.grand_total * doc.conversion_rate,
                               doc.precision("base_grand_total"))
    set_rounded_total(doc)
    doc.set_total_in_words()


def set_rounded_total(doc):
    if doc.meta.get_field("rounded_total"):
        if doc.is_rounded_total_disabled():
            doc.rounded_total = doc.base_rounded_total = 0
            return

        doc.rounded_total = round_based_on_smallest_currency_fraction(
            doc.grand_total, doc.currency, doc.precision("rounded_total"))

        # if print_in_rate is set, we would have already calculated rounding adjustment
        doc.rounding_adjustment += flt(doc.rounded_total - doc.grand_total,
                                       doc.precision("rounding_adjustment"))

        for f in ["rounding_adjustment", "rounded_total"]:
            val = flt(flt(doc.get(f), doc.precision(f)) *
                      doc.conversion_rate, doc.precision("base_" + f))
            doc.set("base_" + f, val)


def update_discounts(doc):
    for table_field in ('pos_discount', 'non_pos_discount'):
        for row in doc.get(table_field, default=[]):
            if not row.discount_rate:
                continue
            if row.discount_type == 'Percentage':
                row.discount_amount = flt(row.invoice_amount) * flt(row.discount_rate) / 100
            elif row.discount_type == 'Fixed':
                row.discount_amount = row.discount_rate


def update_payment_method_total(doc):
    for row in doc.get("payment_method_item", default=[]):
        row.amount = flt(row.pos_amount) + flt(row.non_pos_amount)


@frappe.whitelist()
def update_delivery(doc):
    doc = frappe.parse_json(doc)
    d = frappe.get_doc(doc)
    d._action = "save"
    d.run_method("before_validate")
    d.run_method("validate")
    d.run_method("before_save")
    return d


@frappe.whitelist()
def get_customer_warehouse(customer):
    d = frappe.get_all('Warehouse', [['Warehouse Customer', 'customer', '=', customer]], 'name', limit_page_length=1)
    return d and d[0].name or None
