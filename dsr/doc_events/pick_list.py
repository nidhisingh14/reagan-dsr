from dsr.doc_events.item_location import set_item_location_and_sort


def validate(doc, method=None):
    set_item_location_and_sort(doc, 'locations')
