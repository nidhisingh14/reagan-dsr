import frappe


def set_item_location_and_sort(doc, table_field="items", warehouse_field="warehouse"):
    set_item_location(doc, table_field, warehouse_field)
    doc.set(table_field, sorted(
        doc.get(table_field),
        key=lambda row: (row.zone_location or "", row.bin_location or "", row.item_code)))
    for i, row in enumerate(doc.get(table_field)):
        row.set('idx', i+1)


def set_item_location(doc, table_field="items", warehouse_field="warehouse"):
    for row in doc.get(table_field):
        if not row.item_code:
            continue
        filters = {"parent": row.item_code}
        if warehouse_field and row.get(warehouse_field):
            filters["default_warehouse"] = row.get(warehouse_field)
        location = frappe.get_value(
            "Item Default", filters,
            ["default_zone_location as zone_location", "default_bin_location as bin_location"], as_dict=True)
        if location:
            row.update(location)
