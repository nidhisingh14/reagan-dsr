import frappe


@frappe.whitelist()
def make_stock_entry_approval(
        stock_entry=None,
        stock_entry_type=None,
        source_warehouse=None,
        target_warehouse=None):
    doc = frappe.new_doc("Stock Entry Approval")
    doc.stock_entry_reference = stock_entry
    doc.stock_entry_type = stock_entry_type
    doc.source_warehouse = source_warehouse
    doc.target_warehouse = target_warehouse
    doc.status = "Not Approved"
    doc.insert()
    return doc.name
