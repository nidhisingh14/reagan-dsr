import frappe


@frappe.whitelist()
def check_item_allowed_to_user(user=None, item_code=None):
    """Method 1"""
    status = 0
    condition = ""
    condition += " and it.item_code = '%s'" % item_code
    condition += " and it_item.user_id = '%s'" % user
    data = frappe.db.sql("""
        SELECT
            it_item.user_id
        FROM
            `tabItem` it, `tabAllowed Users` it_item
        WHERE
            it.name = it_item.parent %s;
        """ % condition, as_dict=True)
    if data:
        if data[0].user_id == user:
            status = 1
    return status


@frappe.whitelist()
def filter(doctype, txt, searchfield, start, page_len, filters, menu):
    """Method 2"""
    from frappe.desk.reportview import get_match_cond
    searchfield = "t1.name"
    fcond = " and t2.menu_name = '" + menu + "'"

    return frappe.db.sql(""" select t1.name, item_name from `tabItem` t1, `tabItem Menu` t2
        where t1.name = t2.parent
            and ({key} like %(txt)s
                or item_name like %(txt)s)
            {fcond} {mcond}
        order by
            if(locate(%(_txt)s, t1.name), locate(%(_txt)s, t1.name), 9999),
            if(locate(%(_txt)s, item_name), locate(%(_txt)s, item_name), 9999),
            t1.idx desc,
            t1.name, t1.item_name
        limit %(start)s, %(page_len)s""".format(**{
        'key': searchfield,
        'fcond': fcond,
        'mcond': get_match_cond(doctype)
    }), {
        'txt': "%%%s%%" % txt,
        '_txt': txt.replace("%", ""),
        'start': start,
        'page_len': page_len
    })


@frappe.whitelist()
def filter_result(doctype, txt, searchfield, start, page_len, filters):
    menu = get_menus_based_on_user(user_id=filters['user_id'])
    items = []
    for idx in menu:
        mini_items = filter(doctype, txt, searchfield, start, page_len, filters, idx)
        for jdx in mini_items:
            items.append(jdx)
    return items


def get_menus_based_on_user(user_id=None):
    condition = ""
    condition += " and mf_item.user_id = '%s'" % user_id
    data = frappe.db.sql(
        """
        SELECT
            distinct mf.menu_name
        FROM
            `tabMenu Filter` mf, `tabAllowed Users` mf_item
        WHERE
            mf.name = mf_item.parent %s
        GROUP BY
            mf.menu_name
        ORDER BY
            mf.menu_name
        """ % condition, as_dict=True
    )
    menu = []
    if data:
        for idx in data:
            menu.append(idx.menu_name)
    return menu
