from erpnext.controllers.taxes_and_totals import calculate_taxes_and_totals
from frappe.utils.data import cint, flt


class dsr_calculate_taxes_and_totals(calculate_taxes_and_totals):
    def calculate(self):
        # Reset Previous Calculation.
        for item in self.doc.get("items", default=[]):
            item.net_rate = item.rate
            item.net_amount = item.amount
        return super(dsr_calculate_taxes_and_totals, self).calculate()

    def calculate_net_total(self):
        self.doc.pos_item_qty = self.doc.non_pos_item_qty = self.doc.pos_sales_amount = self.doc.non_pos_sales_amount = self.doc.pos_net_sales_amount = self.doc.non_pos_net_sales_amount = 0.0
        for item in self.doc.get("items"):
            if item.parentfield == "pos_items":
                self.doc.pos_sales_amount += item.amount
                self.doc.pos_item_qty += item.qty
                self.doc.pos_net_sales_amount += item.net_amount
            elif item.parentfield == "non_pos_items":
                self.doc.non_pos_item_qty += item.qty
                self.doc.non_pos_sales_amount += item.amount
                self.doc.non_pos_net_sales_amount += item.net_amount
        super(dsr_calculate_taxes_and_totals, self).calculate_net_total()

    def calculate_taxes(self):
        self.doc.rounding_adjustment = 0
        # POS Items
        self._calculate_taxes('pos_items', {'table_field': ['in', ["pos_discount", "", None]]})
        # NON-POS Items
        self._calculate_taxes("non_pos_items", {'table_field': "non_pos_discount"})

    def _calculate_taxes(self, item_table_field, tax_filters=None):
        # maintain actual tax rate based on idx
        actual_tax_dict = dict([[tax.idx, flt(tax.tax_amount, tax.precision("tax_amount"))]
                                for tax in self.doc.get("taxes", tax_filters) if tax.charge_type == "Actual"])

        for n, item in enumerate(self.doc.get("items", filters={"parentfield": item_table_field})):
            item_tax_map = self._load_item_tax_rate(item.item_tax_rate)
            for i, tax in enumerate(self.doc.get("taxes", tax_filters)):
                # tax_amount represents the amount of tax for the current step
                current_tax_amount = self.get_current_tax_amount(item, tax, item_tax_map)

                # Adjust divisional loss to the last item
                if tax.charge_type == "Actual":
                    actual_tax_dict[tax.idx] -= current_tax_amount
                    if n == len(self.doc.get("items", filters={"parentfield": item_table_field})) - 1:
                        current_tax_amount += actual_tax_dict[tax.idx]

                # accumulate tax amount into tax.tax_amount
                if tax.charge_type != "Actual" and not (
                        self.discount_amount_applied and self.doc.apply_discount_on == "Grand Total"):
                    tax.tax_amount += current_tax_amount

                # store tax_amount for current item as it will be used for
                # charge type = 'On Previous Row Amount'
                tax.tax_amount_for_current_item = current_tax_amount

                # set tax after discount
                tax.tax_amount_after_discount_amount += current_tax_amount

                current_tax_amount = self.get_tax_amount_if_for_valuation_or_deduction(
                    current_tax_amount, tax)

                # note: grand_total_for_current_item contains the contribution of
                # item's amount, previously applied tax and the current tax on that item
                if i == 0:
                    tax.grand_total_for_current_item = flt(item.net_amount + current_tax_amount)
                else:
                    tax.grand_total_for_current_item = flt(
                        self.doc.get("taxes", tax_filters)[i - 1].grand_total_for_current_item +
                        current_tax_amount)

                # set precision in the last item iteration
                if n == len(self.doc.get("items", {"parentfield": item_table_field})) - 1:
                    self.round_off_totals(tax)
                    self.set_cumulative_total(i, tax)

                    self._set_in_company_currency(
                        tax, ["total", "tax_amount", "tax_amount_after_discount_amount"])

                    # adjust Discount Amount loss in last tax iteration
                    if i == (len(self.doc.get("taxes", tax_filters)) - 1) and self.discount_amount_applied \
                            and self.doc.discount_amount and self.doc.apply_discount_on == "Grand Total":
                        self.doc.rounding_adjustment = flt(self.doc.grand_total
                                                           - flt(self.doc.discount_amount) - tax.total,
                                                           self.doc.precision("rounding_adjustment"))

    def determine_exclusive_rate(self):
        self._determine_exclusive_rate(
            'pos_items', {
                'table_field': [
                    'in', [
                        "pos_discount", "", None]]})
        self._determine_exclusive_rate("non_pos_items", {'table_field': "non_pos_discount"})

    def _determine_exclusive_rate(self, item_table_field, tax_filters=None):
        if not any((cint(tax.included_in_print_rate)
                   for tax in self.doc.get("taxes", tax_filters))):
            return

        for item in self.doc.get("items", {"parentfield": item_table_field}):
            item_tax_map = self._load_item_tax_rate(item.item_tax_rate)
            cumulated_tax_fraction = 0
            total_inclusive_tax_amount_per_qty = 0
            for i, tax in enumerate(self.doc.get("taxes", tax_filters)):
                tax.tax_fraction_for_current_item, inclusive_tax_amount_per_qty = self.get_current_tax_fraction(
                    tax, item_tax_map)

                if i == 0:
                    tax.grand_total_fraction_for_current_item = 1 + tax.tax_fraction_for_current_item
                else:
                    tax.grand_total_fraction_for_current_item = self.doc.get(
                        "taxes", tax_filters)[
                        i - 1].grand_total_fraction_for_current_item + tax.tax_fraction_for_current_item

                cumulated_tax_fraction += tax.tax_fraction_for_current_item
                total_inclusive_tax_amount_per_qty += inclusive_tax_amount_per_qty * flt(item.qty)

            if not self.discount_amount_applied and item.qty and (
                    cumulated_tax_fraction or total_inclusive_tax_amount_per_qty):
                amount = flt(item.amount) - total_inclusive_tax_amount_per_qty

                item.net_amount = flt(amount / (1 + cumulated_tax_fraction))
                item.net_rate = flt(item.net_amount / item.qty, item.precision("net_rate"))
                item.discount_percentage = flt(item.discount_percentage,
                                               item.precision("discount_percentage"))

                self._set_in_company_currency(item, ["net_rate", "net_amount"])

    def calculate_totals(self):
        return super(dsr_calculate_taxes_and_totals, self).calculate_totals()
        # TODO:  Set Grand Totals and total_taxes and Charges.

    def set_item_wise_tax(self, item, tax, tax_rate, current_tax_amount):
        # store tax breakup for each item
        key = "{}_{}".format(item.parentfield, item.item_code or item.item_name)
        item_wise_tax_amount = current_tax_amount * self.doc.conversion_rate
        if tax.item_wise_tax_detail.get(key):
            item_wise_tax_amount += tax.item_wise_tax_detail[key][1]

        tax.item_wise_tax_detail[key] = [tax_rate, flt(item_wise_tax_amount)]
    
    def calculate_totals(self):
        # Grand Totals
        self.doc.grand_total_pos = flt(self.doc.get("taxes", {'table_field': ['in', ["pos_discount", "", None]]})[-1].total) + flt(self.doc.rounding_adjustment) \
            if self.doc.get("taxes", {'table_field': ['in', ["pos_discount", "", None]]}) else flt(self.doc.pos_net_sales_amount)
        self.doc.grand_total_non_pos = flt(self.doc.get("taxes", {'table_field': "non_pos_discount"})[-1].total) + flt(self.doc.rounding_adjustment) \
            if self.doc.get("taxes", {'table_field': "non_pos_discount"}) else flt(self.doc.non_pos_net_sales_amount)
        self.doc.grand_total = flt(self.doc.grand_total_pos) + flt(self.doc.grand_total_non_pos)

        # Taxes and Discount
        self.doc.total_taxes_and_charges = self.doc.total_non_pos_discount = self.doc.total_pos_discount = 0.0
        for row in self.doc.get("taxes", default = []):
            if row.table_field == "pos_discount":
                self.doc.total_pos_discount += flt(row.tax_amount)
            elif row.table_field == "non_pos_discount":
                self.doc.total_non_pos_discount += flt(row.tax_amount)
            else:
                self.doc.total_taxes_and_charges += flt(row.tax_amount)

        self._set_in_company_currency(self.doc, ["total_taxes_and_charges", "rounding_adjustment"])

        if self.doc.doctype in ["Quotation", "Sales Order", "Delivery Note", "Sales Invoice"]:
            self.doc.base_grand_total = flt(self.doc.grand_total * self.doc.conversion_rate, self.doc.precision("base_grand_total"))

        self.doc.round_floats_in(self.doc, ["grand_total", "base_grand_total"])

        self.set_rounded_total()

    def set_cumulative_total(self, row_idx, tax):
        tax_amount = tax.tax_amount_after_discount_amount
        tax_amount = self.get_tax_amount_if_for_valuation_or_deduction(tax_amount, tax)

        if row_idx == 0:
            tax.total = flt(flt(self.doc.non_pos_net_sales_amount if tax.table_field == "non_pos_discount" else self.doc.pos_net_sales_amount) + tax_amount, tax.precision("total"))
        else:
            if tax.table_field == "non_pos_discount":
                filters = {"table_field": "non_pos_discount"}
            else:
                filters = {"table_field": ["in", ["pos_discount", "", None]]}
            tax.total = flt(self.doc.get("taxes", filters)[row_idx-1].total + tax_amount, tax.precision("total"))
