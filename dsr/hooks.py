# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "dsr"
app_title = "DSR"
app_publisher = "Mainul Islam"
app_description = "Daily Sales Report"
app_icon = "fa fa-user"
app_color = "green"
app_email = "mainulkhan94@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

fixtures = [
    {
        "dt": "Custom Field",
        "filters": [["app_name", "=", "dsr"]]
    },
    {
        "dt": "Property Setter",
        "filters": [["app_name", "=", "dsr"]]
    }
]

# include js, css files in header of desk.html
# app_include_css = "/assets/dsr/css/dsr.css"
# app_include_js = "/assets/dsr/js/dsr.js"

# include js, css files in header of web template
# web_include_css = "/assets/dsr/css/dsr.css"
# web_include_js = "/assets/dsr/js/dsr.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
doctype_js = {
    "Item": "public/js/form/item.js",
    "Material Request": "public/js/form/material_request.js",
    "Sales Order": "public/js/form/sales_order.js",
    "Stock Reconciliation": "public/js/form/stock_reconciliation.js",
    "Pick List": "public/js/form/pick_list.js",
    "Delivery Note": "public/js/form/delivery_note.js",
    "Sales Invoice": "public/js/form/sales_invoice.js"
}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "dsr.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "dsr.install.before_install"
after_install = "dsr.install.after_install"

after_migrate = "dsr.migrate.after_migrate"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "dsr.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events


doc_events = {
    "Material Request": {
        "validate": "dsr.doc_events.material_request.validate"
    },
    "Sales Order": {
        "validate": "dsr.doc_events.sales_order.validate"
    },
    "Stock Reconciliation": {
        "validate": "dsr.doc_events.stock_reconciliation.validate"
    },
    "Pick List": {
        "before_save": "dsr.doc_events.pick_list.validate"
    },
    "Delivery Note": {
        "onload": "dsr.doc_events.delivery_note.delivery_note_events",
        "before_validate": "dsr.doc_events.delivery_note.delivery_note_events",
        "before_update_after_submit": "dsr.doc_events.delivery_note.delivery_note_events",
        "before_cancel": "dsr.doc_events.delivery_note.delivery_note_events",
        "before_save": "dsr.doc_events.delivery_note.delivery_note_events",
        "before_submit": "dsr.doc_events.delivery_note.delivery_note_events"
    },
    "Sales Invoice": {
        "onload": "dsr.doc_events.sales_invoice.sales_invoice_events",
        "before_validate": "dsr.doc_events.sales_invoice.sales_invoice_events",
        "before_update_after_submit": "dsr.doc_events.sales_invoice.sales_invoice_events",
        "before_cancel": "dsr.doc_events.sales_invoice.sales_invoice_events",
        "before_save": "dsr.doc_events.sales_invoice.sales_invoice_events",
        "before_submit": "dsr.doc_events.sales_invoice.sales_invoice_events"
    }
}

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"dsr.tasks.all"
# 	],
# 	"daily": [
# 		"dsr.tasks.daily"
# 	],
# 	"hourly": [
# 		"dsr.tasks.hourly"
# 	],
# 	"weekly": [
# 		"dsr.tasks.weekly"
# 	]
# 	"monthly": [
# 		"dsr.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "dsr.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "dsr.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "dsr.task.get_dashboard_data"
# }

jenv = {
    "methods": [
        "get_company_address:frappe.contacts.doctype.address.address.get_company_address"
    ]
}
